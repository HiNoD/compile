﻿using System.Collections.Generic;

namespace SLRRunner
{
    using static Enums;
    public static class RunnerConfig
    {
        public const string END_STATE = "End of file";
        public const string END_INPUT = "[End of file]";
        public const string GRAMMAR_TABLE_FILE_NAME = "grammarTable.txt";
        public const string REFERENCE_MAP_FILE_NAME = "referenceMap.txt";

        public static readonly Dictionary<TokenType, string> GRAMMAR_WORDS = new Dictionary<TokenType, string>()
        {
              { TokenType.INT_KEYWORD       , "INT_KEYWORD"    }
            , { TokenType.FLOAT_KEYWORD     , "FLOAT_KEYWORD"  }
            , { TokenType.CHAR_KEYWORD      , "CHAR_KEYWORD"   }
            , { TokenType.STRING_KEYWORD    , "STRING_KEYWORD" }
            , { TokenType.BOOL_KEYWORD      , "BOOL_KEYWORD"   }
            , { TokenType.ARRAY_KEYWORD     , "ARRAY_KEYWORD"  }
            , { TokenType.MAIN_KEYWORD      , "MAIN_KEYWORD"   }
            , { TokenType.WHILE_KEYWORD     , "WHILE_KEYWORD"  }
            , { TokenType.IF_KEYWORD        , "IF_KEYWORD"     }
            , { TokenType.ELSE_KEYWORD      , "ELSE_KEYWORD"   }
            , { TokenType.WRITE_KEYWORD     , "WRITE"          }
            , { TokenType.WRITE_LINE_KEYWORD, "WRITE_LINE"     }
            , { TokenType.READ_KEYWORD      , "READ"           }
            , { TokenType.READ_LINE_KEYWORD , "READ_LINE"      }
            , { TokenType.BRACKET_OPEN      , "BRACKET_OPEN"   }
            , { TokenType.BRACKET_CLOSE     , "BRACKET_CLOSE"  }
            , { TokenType.BLOCK_OPEN        , "BLOCK_OPEN"     }
            , { TokenType.BLOCK_CLOSE       , "BLOCK_CLOSE"    }
            , { TokenType.ARRAY_OPEN        , "ARRAY_OPEN"     }
            , { TokenType.ARRAY_CLOSE       , "ARRAY_CLOSE"    }
            , { TokenType.EOS               , "EOS"            }
            , { TokenType.END_OF_FILE       , "[End of file]"  }
            , { TokenType.IDENTIFIER        , "ID"             }
            , { TokenType.ASSIGNMENT        , "EQUALS"         }
            , { TokenType.PLUS              , "PLUS"           }
            , { TokenType.MINUS             , "MINUS"          }
            , { TokenType.SLASH             , "SLASH"          }
            , { TokenType.STAR              , "STAR"           }
            , { TokenType.COMPARATOR        , "COMPARATOR"     }
            , { TokenType.COMMA             , "COMMA"          }
            , { TokenType.TRUE              , "TRUE"           }
            , { TokenType.FALSE             , "FALSE"          }
            , { TokenType.NOT               , "NOT"            }
            , { TokenType.CHAR              , "CHAR"           }
            , { TokenType.STRING            , "STRING"         }
            , { TokenType.INT               , "INT"            }
            , { TokenType.FLOAT              , "FLOAT"           }
            , { TokenType.OR                , "OR"             }
            , { TokenType.AND               , "AND"            }
        };
    }
}