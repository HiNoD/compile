﻿namespace SLRRunner
{
    public static class Enums
    {
        public enum TokenType
        {
             STATE
           , INT
           , INT_KEYWORD
           , FLOAT
           , FLOAT_KEYWORD
           , CHAR
           , CHAR_KEYWORD
           , STRING
           , STRING_KEYWORD
           , BOOL
           , BOOL_KEYWORD
           , ARRAY
           , ARRAY_KEYWORD
           , MAIN_KEYWORD
           , WHILE_KEYWORD
           , IF_KEYWORD
           , ELSE_KEYWORD
           , WRITE_KEYWORD
           , WRITE_LINE_KEYWORD
           , READ_KEYWORD
           , READ_LINE_KEYWORD
           , BRACKET_OPEN
           , BRACKET_CLOSE
           , BLOCK_OPEN
           , BLOCK_CLOSE
           , ARRAY_OPEN
           , ARRAY_CLOSE
           , EOS
           , END_OF_FILE
           , IDENTIFIER
           , ASSIGNMENT
           , PLUS
           , MINUS
           , UMINUS
           , SLASH
           , STAR
           , COMPARATOR
           , COMMA
           , BRACKET
           , TRUE
           , FALSE
           , AND
           , OR
           , NOT
           , TYPE_STATE
           , BLOCK_CLOSE_STATE
           , ERROR
        }

        public enum Props
        {
            NONE, ADD, SUB, MUL, DIV, CMP, LOGIC, ASSIGN
        }
    }
}
