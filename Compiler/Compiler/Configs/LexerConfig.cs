﻿using System.Collections.Generic;

namespace SLRRunner
{
    using static Enums.TokenType;

    public static class LexerConfig
    {
        public const string LEXICAL_TABLE_FILE = "lexicalTable.txt";

        public static readonly Dictionary<char, string> CharacterTypes = new Dictionary<char, string>
        {
              { '.' , "point"        }
            , { ' ' , "space"        }
            , { ';' , "eos"          }
            , { '+' , "plus"         }
            , { '-' , "minus"        }
            , { '=' , "equal"        }
            , { '/' , "slash"        }
            , { '*' , "star"         }
            , { '(' , "bracket"      }
            , { ')' , "bracket"      }
            , { '[' , "bracket"      }
            , { ']' , "bracket"      }
            , { '{' , "bracket"      }
            , { '}' , "bracket"      }
            , { '>' , "gt"           }
            , { '<' , "gt"           }
            , { '&' , "amp"          }
            , { '|' , "line"         }
            , { '!' , "not"          }
            , { ',' , "comma"        }
            , { '\'', "quote"        }
            , { '\"', "double_quote" }
            , { '\n', "eoln"         }
            , { '\t', "delimiter"    }
            , { '\r', "delimiter"    }
        };

        public static readonly Dictionary<string, Enums.TokenType> TokenNames = new Dictionary<string, Enums.TokenType>
        {
              { "id"               , IDENTIFIER }
            , { "integer"          , INT        }
            , { "end_of_statement" , EOS        }
            , { "float"            , FLOAT      }
            , { "char_end"         , CHAR       }
            , { "string_end"       , STRING     }
            , { "assignment"       , ASSIGNMENT }
            , { "addition"         , PLUS       }
            , { "subtraction"      , MINUS      }
            , { "division"         , SLASH      }
            , { "multiplication"   , STAR       }
            , { "comparator"       , COMPARATOR }
            , { "double_comparator", COMPARATOR }
            , { "comma"            , COMMA      }
            , { "and_end"          , AND        }
            , { "or_end"           , OR         }
            , { "not"              , NOT        }
            , { "bracket"          , BRACKET    }
        };

        public static readonly Dictionary<string, Enums.TokenType> Keywords = new Dictionary<string, Enums.TokenType>
        {
              { "if"       , IF_KEYWORD         }
            , { "else"     , ELSE_KEYWORD       }
            , { "int"      , INT_KEYWORD        }
            , { "float"    , FLOAT_KEYWORD      }
            , { "bool"     , BOOL_KEYWORD       }
            , { "char"     , CHAR_KEYWORD       }
            , { "string"   , STRING_KEYWORD     }
            , { "while"    , WHILE_KEYWORD      }
            , { "array"    , ARRAY_KEYWORD      }
            , { "main"     , MAIN_KEYWORD       }
            , { "Write"    , WRITE_KEYWORD      }
            , { "WriteLine", WRITE_LINE_KEYWORD }
            , { "Read"     , READ_KEYWORD       }
            , { "ReadLine" , READ_LINE_KEYWORD  }
            , { "true"     , TRUE               }
            , { "false"    , FALSE              }
        };
    }
}
