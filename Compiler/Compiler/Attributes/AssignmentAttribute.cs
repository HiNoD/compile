﻿using System;
using SLRRunner.BaseObjects;
using static SLRRunner.Enums;

namespace SLRRunner.Attributes
{
    class AssignmentAttribute : IAttribute
    {
        public IAttribute variable { get; set; }
        public IAttribute assignableValue { get; set; }

        public void AcceptAttribute(IAttribute attribute)
        {
            throw new NotImplementedException();
        }

        public void AcceptToken(Token tok)
        {
            throw new NotImplementedException();
        }

        public void GenerateCode(BaseObjects.CodeGenerator generator, BaseObjects.TableWorker worker)
        {
            VariableInfo info = worker.GetVariableInfo(variable.GetLexeme());
            string variableType = info.type;
            string assignableValueType = assignableValue.GetAttributeType();

            if (assignableValueType == TokenType.IDENTIFIER.ToString())
            {
                assignableValueType = worker.GetVariableInfo(assignableValue.GetLexeme()).type;
            }

            if (!info.isArray)
            {
                assignableValue.GenerateCode(generator, worker);
            }

            if (variableType == "float" && assignableValueType == "int")
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Сonvertion Int to Float");
                Console.ForegroundColor = ConsoleColor.Green;
            }
            else if (variableType == "int" && assignableValueType == "float")
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine("Сonvertion Float to Int");
                Console.ForegroundColor = ConsoleColor.Green;
            }

            if (!info.isArray)
            {
                generator.StoreValueInAddress(worker.GetVariableAddress(variable.GetLexeme()), variableType);
            }
        }

        public string GetAddress()
        {
            throw new NotImplementedException();
        }

        public string GetAttributeType()
        {
            throw new NotImplementedException();
        }

        public string GetLexeme()
        {
            throw new NotImplementedException();
        }

        public void SetAttributeType(Enums.TokenType type)
        {
            throw new NotImplementedException();
        }

        public void SetType(string type)
        {
            throw new NotImplementedException();
        }
    }
}
