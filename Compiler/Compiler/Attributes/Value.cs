﻿using System;

namespace SLRRunner.Attributes
{
    using static Enums;
    class Value : IAttribute
    {
        public Token value { get; set; } = new Token();
        public bool isNegative { get; set; } = false;
        public int address { get; set; } = 0;

        public void AcceptAttribute(IAttribute attribute)
        {
            throw new NotImplementedException();
        }

        public void AcceptToken(Token tok)
        {
            value = tok;
        }

        public void GenerateCode(BaseObjects.CodeGenerator generator, BaseObjects.TableWorker worker)
        {
            if (value.Type == TokenType.IDENTIFIER)
            {
                generator.LoadValueFromAddress(int.Parse(GetAddress()), worker.GetVariableInfo(value.Lex));
            }
            else if (value.Type == TokenType.INT)
            {
                generator.LoadInt(value.Lex);
            }
            else if (value.Type == TokenType.NOT)
            {
                generator.Not();
            }
            else if (value.Type == TokenType.UMINUS)
            {
                generator.Negation();
            }
            else if (value.Type == TokenType.FLOAT)
            {
                generator.LoadFloat(value.Lex);
            }
            else if (value.Type == TokenType.PLUS)
            {
                generator.Add();
            }
            else if (value.Type == TokenType.MINUS)
            {
                generator.Sub();
            }
            else if (value.Type == TokenType.STAR)
            {
                generator.Mul();
            }
            else if (value.Type == TokenType.SLASH)
            {
                generator.Div();
            }
            else if (value.Type == TokenType.STRING)
            {
                generator.LoadString(value.Lex);
            }
            else if (value.Type == TokenType.CHAR)
            {
                char writingValue = value.Lex[1];
                string numberCharValue = ((int)writingValue).ToString();
                generator.LoadInt(numberCharValue);
            }
            else if (value.Type == TokenType.TRUE)
            {
                generator.LoadInt("1");
            }
            else if (value.Type == TokenType.FALSE)
            {
                generator.LoadInt("0");
            }
            else if (value.Type == TokenType.AND)
            {
                generator.And();
            }
            else if (value.Type == TokenType.OR)
            {
                generator.Or();
            }
            else if (value.Type == TokenType.COMPARATOR)
            {
                string condition = value.Lex;
                switch (condition)
                {
                    case ">":
                        generator.Greater();
                        break;
                    case "<":
                        generator.Less();
                        break;
                    case "==":
                        generator.Equal();
                        break;
                    case "!=":
                        generator.Equal();
                        generator.LoadInt("0");
                        generator.Equal();
                        break;
                    case ">=":
                        int count = generator.code.Count;
                        var codeArr = generator.code.ToArray();
                        string firstCode = codeArr[count - 4];
                        string secondCode = codeArr[count - 2];
                        generator.Equal();
                        generator.code.Enqueue(firstCode);
                        generator.ConvertToFloat();
                        generator.code.Enqueue(secondCode);
                        generator.ConvertToFloat();
                        generator.Greater();
                        generator.Or();
                        break;
                    case "<=":
                        // Скопировать последние 4 операции загрузка-конвертация х2
                        int count2 = generator.code.Count;
                        var codeArr2 = generator.code.ToArray();
                        string firstCode2 = codeArr2[count2 - 4];
                        string secondCode2 = codeArr2[count2 - 2];
                        generator.Equal();
                        generator.code.Enqueue(firstCode2);
                        generator.ConvertToFloat();
                        generator.code.Enqueue(secondCode2);
                        generator.ConvertToFloat();
                        generator.Less();
                        generator.Or();
                        break;
                }
            }
        }

        public string GetAddress()
        {
            return address.ToString();
        }

        public string GetAttributeType()
        {
            string type = value.Type.ToString();
            if (type == TokenType.TRUE.ToString() || type == TokenType.FALSE.ToString())
            {
                type = TokenType.BOOL.ToString();
            }
            return type;
        }

        public string GetLexeme()
        {
            return value.Lex;
        }

        public void SetAttributeType(TokenType type)
        {
            value.Type = type;
        }
    }
}