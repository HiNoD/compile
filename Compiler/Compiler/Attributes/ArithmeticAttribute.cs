﻿using System;
using System.Collections.Generic;
using System.Linq;
using static SLRRunner.Enums;

namespace SLRRunner.Attributes
{
    class ArithmeticAttribute : IAttribute, IExpressionAttribute
    {
        public string type { get; set; } = TokenType.FLOAT.ToString();
        public Queue<IAttribute> attributes { get; set; } = new Queue<IAttribute>();

        public void AcceptAttribute(IAttribute attribute)
        {
            throw new NotImplementedException();
        }

        public void AcceptToken(Token tok)
        {
            throw new NotImplementedException();
        }

        public bool AcceptNegation()
        {
            throw new NotImplementedException();
        }

        public void GenerateCode(BaseObjects.CodeGenerator generator, BaseObjects.TableWorker worker)
        {
            foreach (IAttribute attr in attributes)
            {
                if (attr.GetAttributeType() == TokenType.INT.ToString())
                {
                    attr.SetAttributeType(Enums.TokenType.FLOAT);
                }

                attr.GenerateCode(generator, worker);
            }
        }

        public string GetAttributeType()
        {
            return type;
        }

        public string GetLexeme()
        {
            string lex = "";
            foreach (IAttribute attr in attributes)
            {
                lex += attr.GetLexeme();
            }
            return lex;
        }

        public string GetAddress()
        {
            throw new NotImplementedException();
        }

        public void SetType(string type)
        {
            throw new NotImplementedException();
        }

        public void AddAttribute(IAttribute attribute)
        {
            attributes.Enqueue(attribute);
        }

        public Queue<IAttribute> GetAttributes()
        {
            return attributes;
        }

        public void SetAttributeType(Enums.TokenType type)
        {
            throw new NotImplementedException();
        }

        public void AddAttribute(IAttribute attribute, int position)
        {
            var te = attributes.ToList();
            te.Insert(position, attribute);
            attributes = new Queue<IAttribute>(te);
        }
    }
}