﻿using System;
using System.Collections.Generic;

namespace SLRRunner.Attributes
{
    class IdListAttribute : IAttribute
    {
        public List<IAttribute> Identifiers { get; set; } = new List<IAttribute>();

        public void AcceptAttribute(IAttribute attribute)
        {
            Identifiers.Add(attribute);
        }

        public void AcceptToken(Token tok)
        {
            Value val = new Value();
            val.value = tok;
            Identifiers.Add(val);
        }

        public void GenerateCode(BaseObjects.CodeGenerator generator, BaseObjects.TableWorker worker)
        {
            throw new NotImplementedException();
        }

        public string GetAddress()
        {
            throw new NotImplementedException();
        }

        public string GetAttributeType()
        {
            throw new NotImplementedException();
        }

        public string GetLexeme()
        {
            throw new NotImplementedException();
        }

        public void SetAttributeType(Enums.TokenType type)
        {
            throw new NotImplementedException();
        }

        public void SetType(string type)
        {
            throw new NotImplementedException();
        }
    }
}
