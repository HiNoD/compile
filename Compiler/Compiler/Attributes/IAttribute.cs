﻿using static SLRRunner.Enums;

namespace SLRRunner
{
    interface IAttribute
    {
        void AcceptToken(Token tok);
        void AcceptAttribute(IAttribute attribute);
        void GenerateCode(BaseObjects.CodeGenerator generator, BaseObjects.TableWorker worker);
        string GetAttributeType();
        void SetAttributeType(TokenType type);
        string GetLexeme();
        string GetAddress();
    }
}