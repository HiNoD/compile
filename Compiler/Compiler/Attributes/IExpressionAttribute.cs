﻿using System.Collections.Generic;

namespace SLRRunner.Attributes
{
    interface IExpressionAttribute
    {
        void AddAttribute(IAttribute attribute);
        void AddAttribute(IAttribute attribute, int position);
        Queue<IAttribute> GetAttributes();
    }
}
