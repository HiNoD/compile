﻿using System;
using System.Collections.Generic;
using static SLRRunner.Enums;

namespace SLRRunner.Attributes
{
    class VarAttribute : IAttribute
    {
        public string type { get; set; }
        public List<IAttribute> variables { get; set; } = new List<IAttribute>();
        public IAttribute assignableValue { get; set; }

        public void AcceptAttribute(IAttribute attribute)
        {
            assignableValue = attribute;
        }

        public void AcceptToken(Token tok)
        {
            throw new NotImplementedException();
        }

        public void GenerateCode(BaseObjects.CodeGenerator generator, BaseObjects.TableWorker worker)
        {
            //if(!(assignableValue is null))
            //{
            //    foreach (IAttribute var in variables)
            //    {
            //        AssignmentAttribute attribute = new AssignmentAttribute();
            //        attribute.variable = var;
            //        attribute.variable.address = worker.GetVariableAddress(var.GetLexeme());
            //        attribute.assignableValue = assignableValue;

            //        attribute.GenerateCode(generator, worker);
            //    }
            //}
        }

        public string GetAddress()
        {
            throw new NotImplementedException();
        }

        public string GetAttributeType()
        {
            throw new NotImplementedException();
        }

        public string GetLexeme()
        {
            throw new NotImplementedException();
        }

        public void PutInCharacterTable(BaseObjects.TableWorker worker, BaseObjects.CodeGenerator generator)
        {
            foreach (var variable in variables)
            {
                int address = generator.GenerateAddress(type);
                worker.Put(variable.GetLexeme(), type, address);
            }
        }

        public void SetAttributeType(TokenType type)
        {
            throw new NotImplementedException();
        }
    }
}
