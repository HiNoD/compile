﻿using System;
using System.Collections.Generic;
using static SLRRunner.Enums;

namespace SLRRunner.Attributes
{
    class ConditionAttribute : IAttribute, IExpressionAttribute
    {
        public Queue<IAttribute> attributes { get; set; } = new Queue<IAttribute>();

        public Queue<IAttribute> GetAttributes()
        {
            return attributes;
        }

        public void AcceptAttribute(IAttribute attribute)
        {
            throw new NotImplementedException();
        }

        public void AcceptToken(Token tok)
        {
            throw new NotImplementedException();
        }

        public void AddAttribute(IAttribute attribute)
        {
            attributes.Enqueue(attribute);
        }

        public void GenerateCode(BaseObjects.CodeGenerator generator, BaseObjects.TableWorker worker)
        {
            foreach (IAttribute attribute in attributes)
            {
                attribute.GenerateCode(generator, worker);

                if (attribute.GetAttributeType() == TokenType.INT.ToString())
                {
                    generator.ConvertToFloat();
                }
            }

            generator.LoadInt("0");
        }

        public string GetAddress()
        {
            throw new NotImplementedException();
        }

        public string GetAttributeType()
        {
            return TokenType.BOOL.ToString();
        }

        public string GetLexeme()
        {
            string lex = "";
            foreach (var attribute in attributes)
            {
                lex += attribute.GetLexeme();
            }
            return lex;
        }

        public void SetType(string type)
        {
            throw new NotImplementedException();
        }

        public void SetAttributeType(Enums.TokenType type)
        {
            throw new NotImplementedException();
        }

        public void AddAttribute(IAttribute attribute, int position)
        {
            throw new NotImplementedException();
        }
    }
}
