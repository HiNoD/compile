﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SLRRunner
{
    using static Enums.TokenType;
    using static LexerConfig;
    using TransitionTable = List<List<String>>;

    class Lexer
    {
        private TransitionTable Table { get; set; } = new TransitionTable();
        private Dictionary<string, int> StateIndexes { get; set; } = new Dictionary<string, int>();
        private Dictionary<string, int> CharacterTypeIndexes { get; set; } = new Dictionary<string, int>();

        public Position CurrentPosition { get; private set; } = new Position();

        private StreamReader CodeFile { get; set; }

        public Lexer(string inputFile)
        {
            CodeFile = new StreamReader(inputFile);
            ReadTransitionTable(LexerConfig.LEXICAL_TABLE_FILE);
        }

        private string DefineCharType(char ch)
        {
            string type = "";

            if (!CharacterTypes.TryGetValue(ch, out type))
            {
                if (Char.IsLetter(ch) || Char.IsPunctuation(ch) || Char.IsSeparator(ch))
                {
                    type = "alpha";
                }
                else if (Char.IsDigit(ch))
                {
                    type = "number";
                }
            }

            return type;
        }

        private int GetStateIndex(string state)
        {
            return StateIndexes[state];
        }

        private int GetTypeIndex(string str)
        {
            return CharacterTypeIndexes[str];
        }

        private Enums.TokenType DefineBracketType(Token tok)
        {
            string lex = tok.Lex;

            if (tok.Lex == "[")
            {
                return ARRAY_OPEN;
            }
            else if (tok.Lex == "]")
            {
                return ARRAY_CLOSE;
            }
            else if (tok.Lex == "(")
            {
                return BRACKET_OPEN;
            }
            else if (tok.Lex == ")")
            {
                return BRACKET_CLOSE;
            }
            else if (tok.Lex == "{")
            {
                return BLOCK_OPEN;
            }
            else if (tok.Lex == "}")
            {
                return BLOCK_CLOSE;
            }
            else
            {
                return ERROR;
            }
        }

        public KeyValuePair<Token, Position> GetToken()
        {
            Token tok = new Token();
            char ch;

            Position tokStartPos = new Position()
            {
                CurrentString = CurrentPosition.CurrentString,
                CursorPosition = CurrentPosition.CursorPosition
            };

            string currState = "";
            ch = (char)CodeFile.Read();

            if (CodeFile.EndOfStream)
            {
                tok.Lex = "[End of file]";
                tok.Type = END_OF_FILE;
                return new KeyValuePair<Token, Position>(tok, tokStartPos);
            }

            string type = DefineCharType(ch);

            if (type == "eoln")
            {
                CurrentPosition.IncrementCurrentString();
                CurrentPosition.ResetCursorPosition();
            }

            int typeId = GetTypeIndex(type);
            currState = Table[0][typeId];

            string lex = "";
            if (currState != "start")
            {
                lex += ch;
            }

            if (type == "delimiter")
            {
                for (int i = 0; i < 4; i++)
                {
                    CurrentPosition.IncrementCursorPosition();
                }
            }
            else
            {
                CurrentPosition.IncrementCursorPosition();
            }
            if (type == "space")
            {
                tokStartPos = new Position()
                {
                    CurrentString = CurrentPosition.CurrentString,
                    CursorPosition = CurrentPosition.CursorPosition
                };
            }

            while (!CodeFile.EndOfStream && currState != "ERROR" && currState != "OK")
            {
                ch = (char)CodeFile.Peek();
                if (CodeFile.EndOfStream)
                {
                    if (lex == "")
                    {
                        tok.Lex = "[End of file]";
                        tok.Type = END_OF_FILE;
                        return new KeyValuePair<Token, Position>(tok, tokStartPos);
                    }
                    break;
                }

                type = DefineCharType(ch);

                typeId = GetTypeIndex(type);
                int stateId = GetStateIndex(currState);
                string nextState = Table[stateId][typeId];

                if (nextState == "OK")
                {
                    break;
                }
                if (nextState == "line_comment" || nextState == "comment")
                {
                    lex = "";
                    while (nextState != "OK")
                    {
                        currState = nextState;
                        CodeFile.Read();

                        ch = (char)CodeFile.Peek();
                        type = DefineCharType(ch);

                        typeId = GetTypeIndex(type);
                        stateId = GetStateIndex(currState);
                        nextState = Table[stateId][typeId];
                    }

                    currState = "start";
                    continue;
                }

                CodeFile.Read();
                currState = nextState;

                if (type == "eoln")
                {
                    CurrentPosition.IncrementCurrentString();
                    CurrentPosition.ResetCursorPosition();

                    tokStartPos = new Position()
                    {
                        CurrentString = CurrentPosition.CurrentString,
                        CursorPosition = CurrentPosition.CursorPosition
                    };
                }

                if (type == "delimiter")
                {
                    for (int i = 0; i < 4; i++)
                    {
                        CurrentPosition.IncrementCursorPosition();
                    }
                    tokStartPos = new Position()
                    {
                        CurrentString = CurrentPosition.CurrentString,
                        CursorPosition = CurrentPosition.CursorPosition
                    };
                }

                if (nextState != "start")
                {
                    lex += ch;
                    CurrentPosition.IncrementCursorPosition();
                }
            }

            tok.Lex = lex;
            if (TokenNames.ContainsKey(currState))
            {
                tok.Type = TokenNames[currState];
            }

            if (tok.Type == IDENTIFIER && Keywords.ContainsKey(tok.Lex))
            {
                tok.Type = Keywords[tok.Lex];
            }

            if (tok.Type == BRACKET)
            {
                tok.Type = DefineBracketType(tok);
            }
            //Console.WriteLine(tokStartPos.ToString() + ' ' + tok.Lex);//Debug only
            return new KeyValuePair<Token, Position>(tok, tokStartPos);
        }

        public void ReadTransitionTable(string input)
        {
            int characterId = 0;
            int stateId = 0;

            List<string> lines = new List<string>(File.ReadAllLines(input));

            foreach (string line in lines)
            {
                // First string - character types
                if (lines.IndexOf(line) == 0)
                {
                    List<String> types = new List<String>(line.Split('\t'));
                    foreach (string type in types)
                    {
                        CharacterTypeIndexes[type] = characterId;
                        characterId++;
                    }
                }
                else
                {
                    // Раскидали индексы для типов входных символов, заполняем таблицу
                    // Первое слово - имя состояния

                    List<String> words = new List<String>(line.Split('\t'));
                    string stateName = words.First();
                    words.RemoveAt(0);

                    Table.Add(new List<String>());
                    Table[stateId] = words;
                    StateIndexes[stateName] = stateId;
                    stateId++;
                }
            }
        }
    }
}
