﻿namespace SLRRunner
{
    class Position
    {
        public int CurrentString { get; set; } = 1;
        public int CursorPosition { get; set; } = 1;

        public void ResetCursorPosition()
        {
            CursorPosition = 1;
        }

        public void IncrementCurrentString()
        {
            CurrentString++;
        }

        public void IncrementCursorPosition()
        {
            CursorPosition++;
        }

        override
        public string ToString()
        {
            return CurrentString.ToString() + ',' + CursorPosition.ToString();
        }
    }
}
