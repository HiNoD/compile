﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace SLRRunner
{
    using static RunnerConfig;
    using ReferenceMap = Dictionary<int, AttributeRecord>;
    using Table = List<List<String>>;
    class Parser
    {
        private Table Table { get; set; } = new Table();
        private ReferenceMap Map { get; set; } = new ReferenceMap();

        public Table ReadTable(Dictionary<String, int> symbolReferences)
        {
            Table table = new Table();
            List<String> fileLines = new List<String>(File.ReadAllLines(RunnerConfig.GRAMMAR_TABLE_FILE_NAME));
            string inputString = fileLines.First();
            int stateNum = 0;

            for (int i = 0; i < inputString.Length; i++)
            {
                if (inputString[i] == '[')
                {
                    i++;
                    string state = ReadState(inputString, ref i);
                    if (state == END_STATE)
                    {
                        state = END_INPUT;
                    }
                    symbolReferences[state] = stateNum;
                    stateNum++;
                }
            }
            fileLines.RemoveAt(0);

            foreach (string line in fileLines)
            {
                string str = line;
                List<String> tableRow = GetTableRow(ref str);
                table.Add(tableRow);
            }

            return table;
        }

        public ReferenceMap ReadReferenceMap()
        {
            ReferenceMap map = new ReferenceMap();

            List<String> fileLines = new List<string>(File.ReadLines(RunnerConfig.REFERENCE_MAP_FILE_NAME));
            foreach (string line in fileLines)
            {
                string[] arguments = line.Split(' ').ToArray();

                AttributeRecord newRecord = new AttributeRecord();

                int reductionNumber = int.Parse(arguments[0]);
                newRecord.elementsCount = int.Parse(arguments[1]);
                newRecord.toWhat = arguments[2];
                newRecord.attribute = arguments[3];

                map[reductionNumber] = newRecord;
            }

            return map;
        }


        private String GetCellData(ref string inputString, ref int pos)
        {
            string data = "";

            for (; pos < inputString.Length; pos++)
            {
                if (inputString[pos] != ' ')
                {
                    data += inputString[pos];
                }
                else
                {
                    break;
                }
            }

            return data;
        }

        private List<String> GetTableRow(ref string inputString)
        {
            List<String> row = new List<string>();

            for (int i = 0; i < inputString.Length; i++)
            {
                if (inputString[i] != ' ')
                    row.Add(GetCellData(ref inputString, ref i));
            }
            return row;
        }

        private string ReadState(string str, ref int pos)
        {
            string state = "";
            for (; pos < str.Length; pos++)
            {
                if (str[pos] == ']')
                {
                    break;
                }
                state += str[pos];
            }
            return state;
        }


        private string ReadSpecialState(ref string str, ref int pos)
        {
            string state = "";
            for (; pos < str.Length; pos++)
            {
                if (str[pos] == ']')
                {
                    state += str[pos];
                    break;
                }
                state += str[pos];
            }
            return state;
        }

    }
}
