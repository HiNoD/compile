﻿using System;
using System.Collections.Generic;
using System.Linq;
using SLRRunner.Attributes;

namespace SLRRunner
{
    using System.Text.RegularExpressions;
    using SLRRunner.BaseObjects;
    using static Enums.TokenType;
    using static RunnerConfig;
    using static SLRRunner.Enums;
    using AttributeMap = List<AttributeRecord>;
    using ReferenceMap = Dictionary<int, AttributeRecord>;
    using Table = List<List<String>>;

    class Runner
    {
        private Dictionary<String, int> SymbolIndexes { get; set; } = new Dictionary<string, int>();
        private AttributeMap Attributes { get; set; } = new AttributeMap();
        private static OperationsChecker checker = new OperationsChecker();
        private static TableWorker worker = new TableWorker();
        private static CodeGenerator codeGenerator = new CodeGenerator();
        private Parser parser = new Parser();

        private ReferenceMap Map;
        private Table Table;

        private static Stack<IAttribute> attributes = new Stack<IAttribute>();
        private static Stack<Token> statesInTokens = new Stack<Token>();

        static Dictionary<string, Action> actions = new Dictionary<string, Action>
            {
                  { "CreateTable"         , () => worker.CreateNew()                                    }
                , { "DeleteTable"         , () => worker.Delete()                                       }
                , { "CreateVarAttr"       , () => CreateVarAttribute()                                  }
                , { "PutInTable"          , () => PutVariablesInTable()                                 }
                , { "StoreIdentifier"     , () => StoreIdentifier()                                     }
                , { "CheckBinaryOperation", () => DoBinaryOperation()                                   }
                , { "StoreValue"          , () => StoreValue()                                          }
                , { "CheckAssignment"     , () => DoAssignment()                                        }
                , { "ValueNegation"       , () => ValueNegation()                                       }
                , { "LogicalNot"          , () => LogicalNot()                                          }
                , { "StoreCondition"      , () => StoreCondition()                                      }
                , { "GenerateCode"        , () => attributes.Peek().GenerateCode(codeGenerator, worker) }
                , { "GenerateWrite"       , () => GenerateWrite()                                       }
                , { "GenerateWriteLine"   , () => GenerateWriteLine()                                   }
            };


        public Runner()
        {
            Table = parser.ReadTable(SymbolIndexes);
            Map = parser.ReadReferenceMap();
        }

        public void Run(Lexer lexer)
        {
            Stack<string> states = new Stack<string>();
            List<KeyValuePair<Token, Position>> inputTokens = new List<KeyValuePair<Token, Position>>();
            int currState = 0;
            Regex reduction = new Regex("R[0-9]+[0-9]*");
            states.Push("0");

            Token savedTok = new Token();
            Position savedPos = new Position();

            while (true)
            {
                bool deleteToken = true;

                KeyValuePair<Token, Position> tok = lexer.GetToken();

                inputTokens.Add(tok);

                Token currentToken = inputTokens.First().Key;
                Position currentPosition = inputTokens.First().Value;

                string grammarWord;
                if (currentToken.Type == STATE)
                {
                    grammarWord = currentToken.Lex;

                }
                else
                {
                    savedTok = new Token()
                    {
                        Lex = currentToken.Lex,
                        Type = currentToken.Type
                    };

                    savedPos = new Position()
                    {
                        CurrentString = currentPosition.CurrentString,
                        CursorPosition = currentPosition.CursorPosition
                    };
                    grammarWord = GRAMMAR_WORDS[currentToken.Type];

                }

                if (SymbolIndexes.ContainsKey(grammarWord))
                {
                    currState = int.Parse(states.Peek());
                    SymbolIndexes.TryGetValue(grammarWord, out int id);

                    string nextState = Table[currState][id];

                    if (nextState == "OK")
                    {
                        break;
                    }
                    if (nextState == "ERROR")
                    {
                        Console.ForegroundColor = ConsoleColor.Red;
                        Console.WriteLine("Error in " + savedTok.Lex + " : " + savedPos.ToString());
                        Console.ResetColor();
                        return;
                    }
                    else if (reduction.IsMatch(Table[currState][id]))
                    {
                        string currCellValue = Table[currState][id];
                        string reductionNumberStr = currCellValue.Substring(1);
                        int reductionNumber = int.Parse(reductionNumberStr);
                        Reduction(ref states
                            , ref inputTokens
                            , ref statesInTokens
                            , reductionNumber
                            , Map
                            , worker
                            , attributes);
                        deleteToken = false;
                        continue;
                    }
                    else
                    {
                        states.Push(nextState);
                        statesInTokens.Push(currentToken);
                    }
                    if (deleteToken)
                    {
                        inputTokens.RemoveAt(0);
                    }
                }
                else
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine("Error in " + tok.Key.Lex + '\n' + currentPosition.ToString());
                    Console.ResetColor();
                    return;
                }
            }

            if (states.Last() == "0")
            {
                codeGenerator.GenerateCode();
                return;
            }
            else
            {
                throw new Exception("Something went wrong ¯\\_(ツ)_/¯");
            }
        }

        void Reduction(ref Stack<string> states
            , ref List<KeyValuePair<Token, Position>> inputTokens
            , ref Stack<Token> statesInTokens
            , int reduction
            , ReferenceMap map
            , TableWorker worker
            , Stack<IAttribute> attributes)
        {
            AttributeRecord reductionData = new AttributeRecord();
            map.TryGetValue(reduction, out reductionData);

            int deleteFromStack = reductionData.elementsCount;
            string toWhat = reductionData.toWhat;
            string action = reductionData.attribute;

            if (actions.ContainsKey(action))
            {
                actions[action].Invoke();
            }


            for (int i = 0; i < deleteFromStack; i++)
            {
                states.Pop();
                statesInTokens.Pop();
            }

            Token newTok = new Token
            {
                Lex = toWhat,
                Type = STATE
            };

            inputTokens.Insert(0, new KeyValuePair<Token, Position>(newTok, new Position()));//Вставляем в начало списка
        }

        static IAttribute StoreValueFromTokens(IAttribute type, Stack<Token> statesInTokens)
        {
            IAttribute attr = type;
            attr.AcceptToken(statesInTokens.Peek());
            return attr;
        }

        private static void PutDeclaration()
        {
            IAttribute attribute = attributes.Peek();
            if (!(attribute is IdListAttribute)) return;
            IdListAttribute idListAttribute = (IdListAttribute)attributes.Pop();
            VarAttribute varAttribute = (VarAttribute)attributes.Pop();

            foreach (IAttribute identifier in idListAttribute.Identifiers)
            {
                varAttribute.variables.Add(identifier);
            }
            varAttribute.PutInCharacterTable(worker, codeGenerator);
            //varAttribute.GenerateCode(codeGenerator, worker);
        }

        private static void PutDeclarationAndAssignment(Stack<IAttribute> attributes)
        {
            IAttribute assignableValue = attributes.Pop();
            IdListAttribute idListAttribute = (IdListAttribute)attributes.Pop();
            VarAttribute varAttribute = (VarAttribute)attributes.Pop();

            string assignableValueType = assignableValue.GetAttributeType();
            VariableInfo assignableValueInfo = null;
            if (assignableValueType == IDENTIFIER.ToString())
            {
                assignableValueInfo = worker.GetVariableInfo(assignableValue.GetLexeme());
                assignableValueType = worker.GetVariableInfo(assignableValue.GetLexeme()).type;
            }

            if (assignableValueType != varAttribute.type
                && assignableValueType != TokenType.INT.ToString() && assignableValueType != TokenType.FLOAT.ToString()
                && varAttribute.type != TokenType.INT.ToString() && varAttribute.type != TokenType.FLOAT.ToString())
            {
                throw new Exception(String.Format("Type error: Can't assign value of type \"{0}\"" +
                    " to variable with type \"{1}\""
                    , assignableValue.GetAttributeType()
                    , varAttribute.type));
            }

            assignableValue.GenerateCode(codeGenerator, worker);

            foreach (IAttribute identifier in idListAttribute.Identifiers)
            {
                varAttribute.variables.Add(identifier);
            }
            varAttribute.PutInCharacterTable(worker, codeGenerator);
            codeGenerator.StoreValueInIdList(idListAttribute, worker);
        }

        private static void PutVariablesInTable()
        {
            IAttribute attribute = attributes.Peek();
            if (attribute is IdListAttribute)
            {
                PutDeclaration();
                return;
            }

            PutDeclarationAndAssignment(attributes);
        }

        private static void StoreIdentifier()
        {
            // TODO Refactor this
            if (attributes.Count != 0 && attributes.Peek() is IdListAttribute)
            {
                foreach (Token token in statesInTokens)
                {
                    if (token.Type == IDENTIFIER)
                    {
                        IAttribute val = StoreValueFromTokens(new Value(), statesInTokens);
                        attributes.Peek().AcceptAttribute(val);
                        break;
                    }
                }
            }
            else
            {
                IAttribute attr = StoreValueFromTokens(new IdListAttribute(), statesInTokens);
                attributes.Push(attr);
            }
        }

        public static void JoinTwoExpressionAttributes(IExpressionAttribute first
            , IExpressionAttribute second
            , IAttribute operation)
        {
            foreach (var attribute in second.GetAttributes())
            {
                first.AddAttribute(attribute);
            }
            first.AddAttribute(operation);
            attributes.Push((IAttribute)first);
        }

        public static void JoinValueToExpression(IAttribute value
            , IExpressionAttribute expression
            , IAttribute operation)
        {
            var t = expression as IAttribute;
            if (expression.GetAttributes().Last().GetAttributeType() == TokenType.UMINUS.ToString())
            {
                // Положить перед унарным минусом
                // Просто поменять местами
                var newExpression = new ArithmeticAttribute();
                newExpression.AddAttribute(value);
                foreach (var element in expression.GetAttributes())
                {
                    newExpression.AddAttribute(element);
                }

                newExpression.AddAttribute(operation);
                attributes.Push(newExpression as IAttribute);
                return;
            }
            else
            {
                expression.AddAttribute(value);
                expression.AddAttribute(operation);
            }

            attributes.Push(expression as IAttribute);
        }

        private static void ChangeValueType(IAttribute value, TokenType type)
        {
            value.SetAttributeType(type);
        }

        public static void CreateNewArithmeticExpression(IAttribute first
            , IAttribute second
            , IAttribute operation)
        {
            ArithmeticAttribute attribute = new ArithmeticAttribute();

            attribute.attributes.Enqueue(first);
            attribute.attributes.Enqueue(second);
            attribute.attributes.Enqueue(operation);
            attributes.Push(attribute);
        }

        public static void DoBinaryOperation()
        {
            IAttribute second = attributes.Pop();
            IAttribute operation = attributes.Pop();
            IAttribute first = attributes.Pop();

            checker.CheckBinaryOperation(first, second, operation, worker);

            bool bothValuesIsArithmeticExpression = second is ArithmeticAttribute && first is ArithmeticAttribute;
            bool secondValueIsExpression = second is ArithmeticAttribute;
            bool firstValueIsExpression = first is ArithmeticAttribute;
            bool isNewExpression = second is Value;

            if (bothValuesIsArithmeticExpression)
            {
                JoinTwoExpressionAttributes(first as IExpressionAttribute, second as IExpressionAttribute, operation);
            }
            else if (secondValueIsExpression)
            {
                JoinValueToExpression(first, second as IExpressionAttribute, operation);
            }
            else if (firstValueIsExpression)
            {
                JoinValueToExpression(second, first as IExpressionAttribute, operation);
            }
            else if (isNewExpression)
            {
                string type = FLOAT.ToString();
                CreateNewArithmeticExpression(first, second, operation);
            }
        }

        private static void ReserveAddresToVariable(IAttribute attribute)
        {
            int address = worker.GetVariableAddress(attribute.GetLexeme());
            Value id = (Value)attribute;
            id.address = address;
            attribute = id;
        }

        private static void StoreValue()
        {
            IAttribute attribute = StoreValueFromTokens(new Value(), statesInTokens);
            bool isIdentifier = attribute.GetAttributeType() == TokenType.IDENTIFIER.ToString();
            if (isIdentifier) ReserveAddresToVariable(attribute);
            attributes.Push(attribute);
        }

        public static void DoAssignment()
        {
            IAttribute second = attributes.Pop();
            IAttribute operation = attributes.Pop();
            IAttribute first = attributes.Pop();

            checker.CheckBinaryOperation(first, second, operation, worker);

            AssignmentAttribute assignment = new AssignmentAttribute();
            assignment.variable = first;
            assignment.assignableValue = second;
            assignment.GenerateCode(codeGenerator, worker);
        }

        private static IAttribute CreateUnaryMinus()
        {
            Value val = new Value();
            val.value = new Token();
            val.value.Lex = "";
            val.value.Type = UMINUS;
            return val;
        }

        private static void ChangeSign(ArithmeticAttribute expression)
        {
            IAttribute unaryMinus = CreateUnaryMinus();
            expression.attributes.Enqueue(unaryMinus);
            attributes.Push(expression);
        }

        private static void ValueNegation()
        {
            IAttribute attribute = attributes.Pop();
            bool isSingleValue = attribute is Value;
            bool isArithmeticExpression = attribute is ArithmeticAttribute;

            ArithmeticAttribute expression = new ArithmeticAttribute();

            if (isSingleValue)
            {
                IAttribute singleValue = attribute;
                expression.attributes.Enqueue(singleValue);
            }
            else if (isArithmeticExpression)
            {
                expression = (ArithmeticAttribute)attribute;
            }
            ChangeSign(expression);
        }

        private static IAttribute CreateBoolNegation()
        {
            Value val = new Value();
            val.value = new Token();
            val.value.Lex = "";
            val.value.Type = NOT;
            return val;
        }

        private static void LogicalNot()
        {
            IAttribute attribute = attributes.Peek();
            attributes.Pop();

            bool isSingleValue = attribute is Value;
            bool isBoolExpression = attribute is ConditionAttribute;

            ConditionAttribute condition = new ConditionAttribute();

            if (isSingleValue)
            {
                condition.attributes.Enqueue(attribute);
            }
            else if (isBoolExpression)
            {
                condition = (ConditionAttribute)attribute;
            }

            IAttribute negation = CreateBoolNegation();
            condition.attributes.Enqueue(negation);
            attributes.Push(condition);
        }

        private static void CreateNewConditionAttribute(IAttribute first, IAttribute second, IAttribute comparator)
        {
            ConditionAttribute newCondition = new ConditionAttribute();
            newCondition.attributes.Enqueue(first);
            newCondition.attributes.Enqueue(second);
            newCondition.attributes.Enqueue(comparator);
            attributes.Push(newCondition);
        }

        private static void StoreCondition()
        {
            // TODO Refactor this
            ConditionAttribute condition = new ConditionAttribute();
            IAttribute second = attributes.Pop();
            IAttribute comparator = attributes.Pop();
            IAttribute first = attributes.Pop();

            checker.CheckBinaryOperation(first, second, comparator, worker);

            if (second is ConditionAttribute && first is ConditionAttribute)
            {
                JoinTwoExpressionAttributes(first as IExpressionAttribute, second as IExpressionAttribute, comparator);
            }
            else if (second is ConditionAttribute)
            {
                JoinValueToExpression(first, second as IExpressionAttribute, comparator);
            }
            else if (first is ConditionAttribute)
            {
                JoinValueToExpression(second, first as IExpressionAttribute, comparator);
            }
            else
            {
                CreateNewConditionAttribute(first, second, comparator);
            }
        }

        public static void GenerateWrite()
        {
            IAttribute writingValue = attributes.Peek();
            codeGenerator.GenerateWriteStatement(writingValue, worker);
        }

        public static void GenerateWriteLine()
        {
            GenerateWrite();
            codeGenerator.GenerateEndOfLine();
        }

        private static void CreateVarAttribute()
        {
            VarAttribute attribute = new VarAttribute();
            attribute.type = statesInTokens.Peek().Lex.ToUpper();
            attributes.Push(attribute);
        }
    }
}