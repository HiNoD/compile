﻿namespace SLRRunner
{
    using static Enums;

    class Token
    {
        public string Lex { get; set; }
        public TokenType Type { get; set; }
    }
}
