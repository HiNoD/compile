﻿using System;
using System.Collections.Generic;

namespace SLRRunner.BaseObjects
{
    using static Enums;
    class OperationsChecker
    {
        public OperationsChecker()
        {
            TypeTableData.Operations numeric = new TypeTableData.Operations();
            numeric.operations.Add(Enums.Props.ADD);
            numeric.operations.Add(Enums.Props.SUB);
            numeric.operations.Add(Enums.Props.MUL);
            numeric.operations.Add(Enums.Props.DIV);
            numeric.operations.Add(Enums.Props.CMP);
            numeric.operations.Add(Enums.Props.LOGIC);
            numeric.operations.Add(Enums.Props.ASSIGN);

            TypeTableData.Operations char_string = new TypeTableData.Operations();
            char_string.operations.Add(Enums.Props.ADD);

            TypeTableData.Operations string_string = new TypeTableData.Operations();
            string_string.operations.Add(Enums.Props.ADD);
            string_string.operations.Add(Enums.Props.CMP);
            string_string.operations.Add(Enums.Props.ASSIGN);

            TypeTableData.Operations booleans = new TypeTableData.Operations();
            booleans.operations.Add(Enums.Props.LOGIC);
            booleans.operations.Add(Enums.Props.ASSIGN);
            booleans.operations.Add(Enums.Props.CMP);

            TypeTableData.Operations char_char = new TypeTableData.Operations();
            char_char.operations.Add(Enums.Props.CMP);
            char_char.operations.Add(Enums.Props.ASSIGN);

            typeTable = new List<TypeTableData>()
            {
                  { new TypeTableData(TokenType.INT.ToString(), TokenType.INT.ToString(), numeric) }
                , { new TypeTableData(TokenType.INT.ToString(), TokenType.FLOAT.ToString(), numeric) }
                , { new TypeTableData(TokenType.FLOAT.ToString(), TokenType.INT.ToString(), numeric) }
                , { new TypeTableData(TokenType.FLOAT.ToString(), TokenType.FLOAT.ToString(), numeric) }
                , { new TypeTableData(TokenType.CHAR.ToString(), TokenType.STRING.ToString(), char_string) }
                , { new TypeTableData(TokenType.STRING.ToString(), TokenType.CHAR.ToString(), char_string) }
                , { new TypeTableData(TokenType.STRING.ToString(), TokenType.STRING.ToString(), string_string) }
                , { new TypeTableData(TokenType.BOOL.ToString(), TokenType.BOOL.ToString(), booleans)}

                , { new TypeTableData(TokenType.INT.ToString(), TokenType.BOOL.ToString(), booleans) }
                , { new TypeTableData(TokenType.BOOL.ToString(), TokenType.INT.ToString(), booleans) }
                , { new TypeTableData(TokenType.FLOAT.ToString(), TokenType.BOOL.ToString(), booleans) }
                , { new TypeTableData(TokenType.BOOL.ToString(), TokenType.FLOAT.ToString(), booleans) }
                , { new TypeTableData(TokenType.STRING.ToString(), TokenType.BOOL.ToString(), booleans) }
                , { new TypeTableData(TokenType.BOOL.ToString(), TokenType.STRING.ToString(), booleans) }
                , { new TypeTableData(TokenType.CHAR.ToString(), TokenType.BOOL.ToString(), booleans) }
                , { new TypeTableData(TokenType.BOOL.ToString(), TokenType.CHAR.ToString(), booleans) }
                , { new TypeTableData(TokenType.CHAR.ToString(), TokenType.CHAR.ToString(), char_char) }
            };
        }

        private List<TypeTableData> typeTable = new List<TypeTableData>();

        private TypeTableData FindData(string firstType, string secondType)
        {
            foreach (TypeTableData data in typeTable)
            {
                if (data.firstType == firstType
                    && data.secondType == secondType)
                {
                    return data;
                }
            }

            return null;
        }

        Enums.Props TypeToEnum(string type)
        {
            if (type == TokenType.PLUS.ToString())
            {
                return Enums.Props.ADD;
            }
            else if (type == TokenType.MINUS.ToString())
            {
                return Enums.Props.SUB;
            }
            else if (type == TokenType.STAR.ToString())
            {
                return Enums.Props.MUL;
            }
            else if (type == TokenType.SLASH.ToString())
            {
                return Enums.Props.DIV;
            }
            else if (type == TokenType.AND.ToString() || type == TokenType.OR.ToString())
            {
                return Enums.Props.LOGIC;
            }
            else if (type == TokenType.COMPARATOR.ToString())
            {
                return Enums.Props.CMP;
            }
            else if (type == TokenType.ASSIGNMENT.ToString())
            {
                return Enums.Props.ASSIGN;
            }
            else
            {
                return Enums.Props.NONE;
            }
        }

        public void CheckBinaryOperation(IAttribute first
            , IAttribute second
            , IAttribute operation
            , TableWorker worker)
        {
            string firstType = first.GetAttributeType();
            string secondType = second.GetAttributeType();

            if (firstType == TokenType.IDENTIFIER.ToString())
            {
                firstType = worker.GetVariableInfo(first.GetLexeme()).type;
            }

            if (secondType == TokenType.IDENTIFIER.ToString())
            {
                secondType = worker.GetVariableInfo(second.GetLexeme()).type;
            }

            if (String.IsNullOrEmpty(firstType))
            {
                throw new Exception(String.Format("Unknown identifier {0}", first.GetLexeme()));
            }

            if (String.IsNullOrEmpty(secondType))
            {
                throw new Exception(String.Format("Unknown identifier {0}", second.GetLexeme()));
            }

            TypeTableData data = FindData(firstType, secondType);

            if (data is null)
            {
                throw new Exception(String.Format("Can't execute operation {0} on values {1}:{2} and {3}:{4}"
                        , operation.GetLexeme()
                        , first.GetLexeme(), firstType
                        , second.GetLexeme(), secondType));
            }

            var operationType = operation.GetAttributeType();
            if (!data.validOperations.operations.Contains(TypeToEnum(operationType)))
            {
                throw new Exception(String.Format("Can't execute operation {0} on values {1}:{2} and {3}:{4}"
                        , operation.GetLexeme()
                        , first.GetLexeme(), firstType
                        , second.GetLexeme(), secondType));
            }
        }
    }
}
