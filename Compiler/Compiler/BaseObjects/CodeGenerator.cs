﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using SLRRunner.Attributes;
using static SLRRunner.Enums;

namespace SLRRunner.BaseObjects
{
    class CodeGenerator
    {
        private const int STACK_SIZE = 8;
        private string DEFINE_STACK = String.Format(".maxstack {0}", UInt16.MaxValue);
        private const string RETURN = "ret";
        private const string fileName = "source.il";
        private Queue<string> LOCALS_CONTENT = new Queue<string>();

        public Queue<string> code { get; set; } = new Queue<string>();
        private HashSet<int> addresses = new HashSet<int>();
        private int currentAddressCount = -1;
        private int mark { get; set; } = 0;
        private List<int> currentMarks = new List<int>();
        private const string MARK_PREFIX = "L";
        private const string MARK_POSTFIX = ":";
        private const string JMP = "br ";

        public void GenerateEndOfLine()
        {
            code.Enqueue("ldstr \"\"");
            code.Enqueue("call void [mscorlib]System.Console::WriteLine(class System.String)");
        }

        public void LoadInt(string number)
        {
            code.Enqueue(String.Format("ldc.i4 {0}", number));
        }

        public void LoadFloat(string number)
        {
            code.Enqueue(String.Format("ldc.r4 {0}", number));
        }

        public void Add()
        {
            code.Enqueue("add");
        }

        public void Sub()
        {
            code.Enqueue("sub");
        }

        public void Div()
        {
            code.Enqueue("div");
        }

        public void Mul()
        {
            code.Enqueue("mul");
        }

        public void Not()
        {
            code.Enqueue("not");
        }

        public void Negation()
        {
            code.Enqueue("neg");
        }

        public void LoadString(string line)
        {
            code.Enqueue(String.Format("ldstr {0}", line));
        }

        public void And()
        {
            code.Enqueue("and");
        }

        public void Or()
        {
            code.Enqueue("or");
        }

        public void Greater()
        {
            code.Enqueue("cgt");
        }

        public void Less()
        {
            code.Enqueue("clt");
        }

        public void Equal()
        {
            code.Enqueue("ceq");
        }

        public void GenerateWriteStatement(IAttribute writingValue, TableWorker worker)
        {
            string statement = "";
            if (writingValue.GetAttributeType() == TokenType.FLOAT.ToString()
                || writingValue.GetAttributeType() == TokenType.INT.ToString()
                || writingValue.GetAttributeType() == TokenType.BOOL.ToString())
            {
                statement = "call void [mscorlib]System.Console::Write(float32)";
            }
            else if (writingValue.GetAttributeType() == TokenType.STRING.ToString())
            {
                statement = "call void [mscorlib]System.Console::Write(class System.String)";
            }
            else if (writingValue.GetAttributeType() == TokenType.CHAR.ToString())
            {
                statement = "call void [mscorlib]System.Console::Write(char)";
            }
            else if (writingValue.GetAttributeType() == TokenType.IDENTIFIER.ToString())
            {
                var info = worker.GetVariableInfo(writingValue.GetLexeme());
                string type = info.type;

                Value val = new Value();
                if (type == TokenType.INT.ToString() || type == TokenType.BOOL.ToString())
                {
                    ConvertToFloat();
                }


                if (type == TokenType.INT.ToString())
                {
                    val.value.Type = Enums.TokenType.INT;
                }
                else if (type == TokenType.FLOAT.ToString())
                {
                    val.value.Type = Enums.TokenType.FLOAT;
                }
                else if (type == TokenType.STRING.ToString())
                {
                    val.value.Type = Enums.TokenType.STRING;
                }
                else if (type == TokenType.CHAR.ToString())
                {
                    val.value.Type = Enums.TokenType.CHAR;
                }
                else if (type == TokenType.BOOL.ToString())
                {
                    val.value.Type = Enums.TokenType.BOOL;
                }

                GenerateWriteStatement(val, worker);
                return;
            }
            code.Enqueue(statement);
        }

        public void Store1DArrayValue(string type)
        {
            if (type == TokenType.INT.ToString()
                || type == TokenType.CHAR.ToString()
                || type == TokenType.BOOL.ToString())
            {
                code.Enqueue("stelem.i4");
            }
            else if (type == TokenType.FLOAT.ToString())
            {
                code.Enqueue("stelem.r4");
            }
            if (type == TokenType.STRING.ToString())
            {
                code.Enqueue("stelem.ref");
            }
        }

        public void StoreValueInIdList(IdListAttribute idList, TableWorker worker)
        {
            IAttribute firstVariable = idList.Identifiers.First();

            int addressToStore = worker.GetVariableAddress(firstVariable.GetLexeme());
            int addressToLoad = addressToStore;

            foreach (var id in idList.Identifiers)
            {
                addressToStore = worker.GetVariableAddress(id.GetLexeme());
                string variableType = worker.GetVariableInfo(id.GetLexeme()).type;
                StoreValueInAddress(addressToStore, variableType);

                LoadValueFromAddress(addressToLoad, worker.GetVariableInfo(id.GetLexeme()));
            }
            Pop();
        }

        public void ConvertTo(string type)
        {
            if (type == TokenType.INT.ToString() || type == TokenType.CHAR.ToString())
            {
                ConvertToInt();
            }
            else if (type == TokenType.FLOAT.ToString())
            {
                ConvertToFloat();
            }
        }

        public void ConvertToInt()
        {
            code.Enqueue("conv.i4");
        }

        public void ConvertToFloat()
        {
            code.Enqueue("conv.r4");
        }

        public void ReadChar()
        {
            code.Enqueue("call int32[mscorlib]System.Console::Read()");
            code.Enqueue("conv.u2");
        }

        public void GenerateReadLine()
        {
            code.Enqueue("call string [mscorlib]System.Console::ReadLine()");
        }

        public void WriteInt()
        {
            code.Enqueue("call int32[mscorlib]System.Int32::Parse(string)");
        }

        public void WriteFloat()
        {
            code.Enqueue("call float32 [mscorlib]System.Single::Parse(string)");
        }

        public void ParseLineTo(string type)
        {
            if (type == TokenType.INT.ToString())
            {
                WriteInt();
            }
            else if (type == TokenType.FLOAT.ToString())
            {
                WriteFloat();
            }
            else if (type == TokenType.CHAR.ToString())
            {
                throw new Exception("Can't assign string to char!");
            }
        }

        public void StoreValueInAddress(int address, string valueType, bool isArray = false)
        {
            if (!isArray && valueType == TokenType.INT.ToString())
            {
                ConvertToInt();
            }
            else if (!isArray && valueType == TokenType.FLOAT.ToString())
            {
                ConvertToFloat();
            }
            code.Enqueue(String.Format("stloc {0}", address));
        }

        public void LoadValueFromAddress(int address, VariableInfo info)
        {
            code.Enqueue(String.Format("ldloc {0}", address));
            if (!info.isArray && info.type == TokenType.INT.ToString())
            {
                ConvertToFloat();
            }
        }

        public void SaveValueInArray(string type, int dimensions)
        {
            string arrayType = "";
            if (type == TokenType.INT.ToString())
            {
                arrayType = "int32";
            }
            else if (type == TokenType.FLOAT.ToString())
            {
                arrayType = "float32";
            }
            else if (type == TokenType.CHAR.ToString())
            {
                arrayType = "int32";
            }
            else if (type == TokenType.STRING.ToString())
            {
                arrayType = "string";
            }
            else if (type == TokenType.BOOL.ToString())
            {
                arrayType = "int32";
            }

            string dimensionsString = "";
            List<string> storage = new List<string>();
            for (int i = 0; i < dimensions; i++)
            {
                storage.Add("0...");
            }
            dimensionsString = String.Join(",", storage);
            storage.Clear();

            string argumentsCount = "";
            for (int i = 0; i < dimensions; i++)
            {
                storage.Add("int32");
            }
            argumentsCount = String.Join(",", storage);

            string saveCommand = String.Format("{call instance void {0}[{1}]::Set({2})}"
                , arrayType, dimensionsString, argumentsCount);
        }

        public void Pop()
        {
            code.Enqueue("pop");
        }

        public int GenerateAddress(string variableType)
        {
            currentAddressCount++;

            string typeStatement = "";

            if (variableType == TokenType.INT.ToString())
            {
                typeStatement = "int32";
            }
            else if (variableType == TokenType.FLOAT.ToString())
            {
                typeStatement = "float32";
            }
            else if (variableType == TokenType.CHAR.ToString() || variableType == TokenType.STRING.ToString())
            {
                typeStatement = "string";
            }
            else if (variableType == TokenType.BOOL.ToString())
            {
                typeStatement = "int32";
            }

            LOCALS_CONTENT.Enqueue(String.Format("[{0}] {1}", currentAddressCount.ToString(), typeStatement));
            return currentAddressCount;
        }

        public void GenerateCode()
        {
            using (StreamWriter sw = new StreamWriter(fileName, false, System.Text.Encoding.Default))
            {
                sw.Write(@".assembly WriteString {}

.assembly extern mscorlib
{
    .publickeytoken = (B7 7A 5C 56 19 34 E0 89)
    .ver 4:0:0:0
}

.method static public void main() il managed
{
.entrypoint
.maxstack 65535
.locals init(");
                while (LOCALS_CONTENT.Count != 0)
                {
                    string local = LOCALS_CONTENT.Dequeue();
                    if (LOCALS_CONTENT.Count != 0)
                    {
                        local += ",";
                    }
                    sw.Write(local);
                }
                sw.WriteLine(")");

                foreach (string codeString in code)
                {
                    sw.WriteLine($"    {codeString}");
                }

                sw.WriteLine(@"
    ldstr ""Press Enter to continue""
    call void [mscorlib]System.Console::WriteLine(class System.String)
    call int32[mscorlib]System.Console::Read()
    pop
    ret
}");
            }
        }
    }
}
