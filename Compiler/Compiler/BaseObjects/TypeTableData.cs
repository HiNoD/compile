﻿using System.Collections.Generic;

namespace SLRRunner.BaseObjects
{
    class TypeTableData
    {
        public class Operations
        {
            public HashSet<Enums.Props> operations { get; set; } = new HashSet<Enums.Props>();
        }

        public TypeTableData(string firstType, string secondType, Operations operations)
        {
            this.firstType = firstType;
            this.secondType = secondType;
            this.validOperations = operations;
        }

        public string firstType { get; set; }
        public string secondType { get; set; }
        public Operations validOperations { get; set; }
    }
}