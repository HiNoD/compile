﻿using System.Collections.Generic;

namespace SLRRunner.BaseObjects
{
    class VariableInfo
    {
        public string type { get; set; }
        public int address { get; set; }
        public bool isArray { get; set; } = false;
        public List<int> dimensions { get; set; } = new List<int>();
    }
}
