﻿using System;
using System.Collections.Generic;

namespace SLRRunner.BaseObjects
{
    class TableWorker
    {
        Stack<CharacterTable> tables = new Stack<CharacterTable>();

        public void CreateNew()
        {
            tables.Push(new CharacterTable());
        }

        public void Delete()
        {
            tables.Pop();
        }

        public int GetVariableAddress(string variable)
        {
            int address = 0;
            foreach (CharacterTable table in tables)
            {
                try
                {
                    address = table.Get(variable).address;
                    if (address != 0)
                    {
                        break;
                    }
                }
                catch (Exception)
                {
                    continue;
                }
            }

            return address;
        }

        public VariableInfo GetVariableInfo(string variable)
        {
            VariableInfo info = null;
            foreach (CharacterTable table in tables)
            {
                try
                {
                    info = table.Get(variable);
                    if (!(info is null))
                    {
                        break;
                    }
                }
                catch (Exception)
                {
                    continue;
                }
            }

            if (info is null)
            {
                throw new Exception(String.Format("Unknown variable {0}", variable));
            }

            return info;
        }

        public void Put(string variable, string type, int address, bool isArray = false, List<int> dimensions = null)
        {
            tables.Peek().Put(variable, type, address, isArray, dimensions);
        }
    }
}
