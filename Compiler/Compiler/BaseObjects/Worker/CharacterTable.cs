﻿using System;
using System.Collections.Generic;
using SLRRunner.BaseObjects;

namespace SLRRunner
{
    class CharacterTable
    {
        private Dictionary<string, VariableInfo> Content { get; set; } = new Dictionary<string, VariableInfo>();

        public void Put(string var, string type, int address
            , bool isArray
            , List<int> dimensions)
        {
            if (!Content.ContainsKey(var))
            {
                VariableInfo info = new VariableInfo();
                info.type = type;
                info.address = address;
                info.isArray = isArray;
                info.dimensions = dimensions;
                Content[var] = info;
            }
            else
            {
                throw new Exception(String.Format("Variable \"{0} {1}\" alredy defined with type {2}!", type, var, Content[var].type));
            }
        }

        public VariableInfo Get(string var)
        {
            if (Content.ContainsKey(var))
            {
                return Content[var];
            }
            else
            {
                throw new Exception("Variable alredy defined!");
            }
        }
    }
}
