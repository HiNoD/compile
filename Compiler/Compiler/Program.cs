﻿using System;

namespace SLRRunner
{
    class Program
    {
        static int Main(string[] args)
        {
            Lexer lexer = new Lexer("source.txt");
            Runner runner = new Runner();

            Console.ForegroundColor = ConsoleColor.Green;

            try
            {
                runner.Run(lexer);
            }
            catch (Exception ex)
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine(ex.Message);
                Console.ResetColor();
            }

            Console.ResetColor();

            return 0;
        }
    }
}
